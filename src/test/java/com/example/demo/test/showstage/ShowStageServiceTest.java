package com.example.demo.test.showstage;

import java.util.List;


import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import com.example.demo.showstage.dto.ShowStageDTO;
import com.example.demo.showstage.service.ShowStageService;

@SpringBootTest
public class ShowStageServiceTest {

	@Autowired
	ShowStageService service;

//	@Test
//	public void 공연장게시물30개추가() {
//		for(int i=0; i<30; i++) {
//			service.register(new ShowStageDTO(0, "공연장이름", "장소", "사이트", "전화번호",null,"", null, null, null, null,null, ""));
//		}
//	}
	
	@Test
	public void 일번페이지_목록조회하기() {
		Page<ShowStageDTO> page = service.getList(1);
		List<ShowStageDTO> list = page.getContent();
		for(ShowStageDTO dto : list) {
			System.out.println(dto);
		}
	}
	
}
